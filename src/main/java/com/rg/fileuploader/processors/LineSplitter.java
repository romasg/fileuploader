package com.rg.fileuploader.processors;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

public class LineSplitter implements Runnable {
    private final static String REPLACEMENT_PATTERN = "[\\W_0-9]";

    private final ConcurrentMap<String, AtomicInteger> myMap;
    String line;

    public LineSplitter(ConcurrentMap<String, AtomicInteger> wordMap, String line) {
        this.myMap = wordMap;
        this.line = line;
    }


    @Override
    public void run() {
        splitLine(line);
    }

    public void splitLine(String line) {
        line.split("\\s");
        for (String word : line.split("\\s")) {
            putIn(cleanChars(word));
        }
    }

    private void putIn(String word) {

        if (word.length() < 1) {
            return;
        }

        AtomicInteger value = myMap.get(word);
        if (value == null) {
            myMap.put(word, new AtomicInteger(1));
        } else {
            value.getAndIncrement();
            myMap.put(word, value);
        }
    }

    private static String cleanChars(String word) {
        return (word.replaceAll(REPLACEMENT_PATTERN, "").toLowerCase());
    }
}
