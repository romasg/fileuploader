package com.rg.fileuploader.processors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class FileProcessor {
    private final ConcurrentMap<String, AtomicInteger> wordMap = new ConcurrentHashMap<>();

    private static final int NUMBER_OF_THREADS=10;

    @Autowired
    private Writer writer;

    private static FileProcessor instance = null;

    @Bean(name = {"processor"})
    public static FileProcessor getInstance() {
        if (instance == null) {
            instance = new FileProcessor();
        }
        instance.wordMap.clear();
        return instance;
    }

    public String processMultipleFiles(MultipartFile[] files) throws IOException, InterruptedException {

        ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
        List<Future<?>> list = new ArrayList<Future<?>>();


        for (MultipartFile file : files) {
            if (!file.isEmpty()) {
                byte[] bytes = file.getBytes();
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

                BufferedReader bufferedReader = null;
                try {
                    bufferedReader = new BufferedReader(new InputStreamReader(byteArrayInputStream));
                    String temp = null;
                    while ((temp = bufferedReader.readLine()) != null) {
                        Future<?> future = executorService.submit(new LineSplitter(wordMap, temp));
                        list.add(future);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        for (Future<?> fut : list) {
            try {
                fut.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        executorService.shutdown();
        this.writer.writeAllMapRezultsToFile(wordMap);

        return printAllMapRezults(true);
    }


    private String printAllMapRezults(boolean forWeb) {
        StringBuilder sb = new StringBuilder();
        sb.append(printMapRezults(wordMap, forWeb));

        return sb.toString();
    }

    private static String printMapRezults(Map<String, AtomicInteger> map, boolean forWeb) {
        Map<String, AtomicInteger> sortedMap = new TreeMap<String, AtomicInteger>(map);
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, AtomicInteger> entry : sortedMap.entrySet()) {
            String key = entry.getKey();
            AtomicInteger value = entry.getValue();
            sb.append(key + "\t\t" + value + (forWeb ? "</br>" : "\n\r"));
        }

        return sb.toString();
    }

}


