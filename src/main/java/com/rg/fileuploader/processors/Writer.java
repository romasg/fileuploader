package com.rg.fileuploader.processors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class Writer {

    @Value("${outputFile}")
    private String outputFile;

    public void writeAllMapRezultsToFile(ConcurrentMap<String, AtomicInteger> wordMap) throws IOException {
        String fileAG = outputFile.replace(".txt", "_AG.txt");
        String fileHN = outputFile.replace(".txt", "_HN.txt");
        String fileOU = outputFile.replace(".txt", "_OU.txt");
        String fileVZ = outputFile.replace(".txt", "_VZ.txt");

        if (!wordMap.isEmpty()) {
            createFile(fileAG);
            createFile(fileHN);
            createFile(fileOU);
            createFile(fileVZ);
        }
        Map<String, AtomicInteger> sortedMap = new TreeMap<String, AtomicInteger>(wordMap);
        StringBuilder sb = new StringBuilder();

        for (Map.Entry<String, AtomicInteger> entry : sortedMap.entrySet()) {
            String key = entry.getKey();
            AtomicInteger value = entry.getValue();

            String firstChar = key.substring(0, 1);

            if (firstChar.matches("[a-g]")) {
                writeLineToFile(fileAG, key + "\t\t" + value + "\r\n");
            } else if (firstChar.matches("[h-n]")) {
                writeLineToFile(fileHN, key + "\t\t" + value + "\r\n");
            } else if (firstChar.matches("[o-u]")) {
                writeLineToFile(fileOU, key + "\t\t" + value + "\r\n");
            } else if (firstChar.matches("[v-z]")) {
                writeLineToFile(fileVZ, key + "\t\t" + value + "\r\n");
            } else {
                return;
            }

            sb.append(key + "\t\t" + value + "\n");


        }
    }

    private static void writeLineToFile(String fileName, String toAppend) {

        try {

            Files.write(Paths.get(fileName), toAppend.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createFile(String fileName) throws IOException {
        File file = new File(fileName);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();

    }
}
