package com.rg.fileuploader.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import com.rg.fileuploader.processors.FileProcessor;

@SpringBootApplication
@ComponentScan(basePackages = {"com.rg.fileuploader.processors", "com.rg.fileuploader.controllers"})
public class Application {

    public static void main(String[] args) {

        SpringApplication app = new SpringApplication(Application.class);
        app.setShowBanner(false);
        ConfigurableApplicationContext context = app.run(args);
        FileProcessor task = (FileProcessor)context.getBean("processor");
    }
}