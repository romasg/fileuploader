package com.rg.fileuploader.controllers;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.rg.fileuploader.processors.FileProcessor;

import java.io.IOException;

@Controller
public class FileUploadController {
    String result;

    @RequestMapping(value = "/result", method = RequestMethod.GET)
    public
    @ResponseBody
    String provideUploadInfo() {
        return this.result;
    }

    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public
    @ResponseBody
    String handleFileUpload(@RequestParam("file") MultipartFile[] files) throws InterruptedException {
        if(files.length==0||files[0].getOriginalFilename().isEmpty()){
            return "No files selected!";
        }
        try {
            String res = FileProcessor.getInstance().processMultipleFiles(files);
            return res;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Upload failed.";
    }

}